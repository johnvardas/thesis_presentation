(TeX-add-style-hook
 "main"
 (lambda ()
   (TeX-add-to-alist 'LaTeX-provided-class-options
                     '(("beamer" "17pt" "xcolor={x11names,svgnames,dvipsnames}")))
   (TeX-add-to-alist 'LaTeX-provided-package-options
                     '(("babel" "british") ("tipa" "T1" "safe") ("inputenc" "utf8") ("fontenc" "LGR" "T1") ("beramono" "scaled=.77") ("biblatex" "style=authoryear-comp" "backend=biber")))
   (TeX-run-style-hooks
    "latex2e"
    "slurm_architecture"
    "tofa_slurm"
    "evaluation"
    "silence"
    "beamer"
    "beamer10"
    "pgfpages"
    "qrcode"
    "babel"
    "subfig"
    "wrapfig"
    "tipa"
    "microtype"
    "inputenc"
    "fontenc"
    "libertine"
    "beramono"
    "relsize"
    "tabularx"
    "hologo"
    "textcomp"
    "comment"
    "multicol"
    "booktabs"
    "listings"
    "algorithm2e"
    "caption"
    "tikz"
    "pgfgantt"
    "multirow"
    "texshade"
    "smartdiagram"
    "bytefield"
    "bbm"
    "upgreek"
    "bookmark"
    "biblatex")
   (TeX-add-symbols
    '("textgreek" 1)
    '("wb" 1))
   (LaTeX-add-bibliographies
    "bibliography"))
 :latex)

