thesis:
	pdflatex main.tex
	biber main
	pdflatex main.tex
	pdflatex main.tex


update:
	git status
	git pull

commit:
	git status
	git commit -a
	git push


clean:
	-rm -f *.log
	-rm -f *.aux
	-rm -f *.bbl
	-rm -f *.blg
	-rm -f *.idx
	-rm -f *.lot
	-rm -f *.lof
	-rm -f *.lot
	-rm -f *.toc
	-rm -f *~
	-rm -rf _*
	-rm -rf *.out
	-rm -rf *.nav
	-rm -rf *.snm
	-rm -rf *.vrb
	-rm -rf *.run.xml
	-rm -rf *.bcf
