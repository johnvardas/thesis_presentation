
# Organization of Slides
## Total Num of slides = 30
* 2 Intro/Motivation - Problem Statement
* 5 Slurm
  * 1 Figure with architecture
  * 1 Plugins
  * 1 Slurm configuration file - **Might not need this**
  * 1 Figure Job execution in Slurm
  * 1 Figure Task launch in Slurm
* 1 Acn-Slurm
* 5 VIMA
  * 1 Other approaches **This is missing**
  * 1 Our approach
  * 1 Implementation
  * 1 Figure VIMA
  * 1 Figure task Launch VIMA
* 18 Slides for TOFA+EVAL
  * 2 Intro/Motivation - Problem Statement
    * 1 Bullets
    * 1 Figure Parallel Programs
  * 1 Related
  * 3 Our approach
  * 1 Slurm Implementation
  * 1 Figure with Slurm Implementation
  * 1 Simulation Platform - Topology and Characteristics
  * 4 No fault eval - DT, CG, HPL, LAMMPS (Group them?)
  * 1 Fault Model
  * 4 DT, CG, HPL, LAMMPS (Group them?)
* 1 Summary

### Further subdivisions for this will be made


# Notes for the presentation - need to discuss
- *Topology Mapping and Graph Embedding*
  - Generally in bibliography those terms are used interchangeably
  
  
### Notes on on Hoefler paper - 2011
- *Topology mapping is NP-complete - Hoefler 2011*
  - Discuss the figure 1 routing. Why it results in congestion 2?
  - The authors attempt to optimize both communication but also congestion
  - Figure 3b is the adjacency matrix for an 8x8x8 Torus - this explains the behaviour LAMMPS and HPL
  - The authors use Recursive Bisection Mapping + Greedy Heuristic + Graph Similarity + Threshold Accepting to produce a mapping
  - *All processes apply the optimization process???* what does that even mean
  
### Notes on Larsson SC-2002 paper
- *MPI Topology mapping problem is to find an embedding of G to H: Larsson - SC-2002*
- Two relevant optimization criteria:
  1. Minimizing total Communication Costs by a mapping 
  2. Optimizing Load Balance by a mapping
- These are NP-complete BUT with the exception, if H is complete? - TI BOURDELO EINAI AUTO TO PAPER?
- *We aim to show that the embedding problems for hierarchical systems can be solved by (weighted) graph partitioning with generalized cost function*
- The authors use graph partioning and k-partitioning to find the embeddings
- The graph embedding problems are NP-hard problems, researchers have focused on developing heuristics
- The problem of mapping application’s task topology onto underneath hardware’s physical topology can be formalized as a graph embedding problem 
- Thus the topology mapping problem is NP-hard (which includes NP-complete)

## TODO
### Notes on Mapping Techniques Survey Hoefler-Jeannot - 2013
 
### Questions
- Can we derive that the topology mapping is NP-Complete or NP-Hard?
- What happens in multicore nodes?
  - An extra partitioning step before mapping should be made
      - Partition Guest Graph into k partitions where k = |G|/p (p is the number of processes executed by a single node)
      - The new Guest Graph has {0, k-1} vertices and only inter-partition edges remain from Guest

## Parallel Programs

- Ocean motions simulations
- n-body problems simulating galaxies, proteins or other molecules

### See the figure

- task = the smallest unit of concurency that a parallel program can exploit
- process = is the entity that performs tasks and communication
- processor = the physical computational resource where a process is executed


# Progress
- **Need Intro + Motivation**
- Slurm + VIMA slides all done
 - Config File might need some more (topology, task affinity?)
 - Discussion about Slurm entities, Partition, Job, Job Step
 - Still missing Related in VIMA
